import React from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import Table from "./components/table";

import './app.css';

const store = configureStore()

const App = () => {
  return (
      <Provider store={store}>
        <div className="App">
            <Table />
        </div>
      </Provider>
  );
}

export default App;
